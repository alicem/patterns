/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: MIT or LGPL-2.1-or-later
 */

#include "patterns-config.h"
#include "patterns-page-utility-pane.h"

#include "patterns-utils.h"

struct _PatternsPageUtilityPane
{
  AdwNavigationPage parent_instance;

  AdwDialog *demo;
};

G_DEFINE_TYPE (PatternsPageUtilityPane, patterns_page_utility_pane, ADW_TYPE_NAVIGATION_PAGE)

static void
demo_activate_cb (PatternsPageUtilityPane *self,
                  const char              *action_name,
                  GVariant                *parameter)
{
  adw_dialog_present (self->demo, GTK_WIDGET (self));
}

static GtkPackType
get_sidebar_position (PatternsPageUtilityPane *self,
                      gboolean                 is_start)
{
  return is_start ? GTK_PACK_START : GTK_PACK_END;
}

static void
patterns_page_utility_pane_class_init (PatternsPageUtilityPaneClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_install_action (widget_class, "demo.run", NULL,
                                   (GtkWidgetActionActivateFunc) demo_activate_cb);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Patterns/pages/utility-pane/patterns-page-utility-pane.ui");
  gtk_widget_class_bind_template_child (widget_class, PatternsPageUtilityPane, demo);
  gtk_widget_class_bind_template_callback (widget_class, get_sidebar_position);
}

static void
patterns_page_utility_pane_init (PatternsPageUtilityPane *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
